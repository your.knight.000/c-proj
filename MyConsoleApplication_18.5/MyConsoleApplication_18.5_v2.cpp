
#include <iostream>

int N;

class Stack
{
private:
 
    int top;

    //int N = 1000;
    int* s = new int[N];

public: 

    Stack() : top(0)
    {}

    ~Stack()
    {
        delete []s;
    }
   
    void push(int var)
    {
        top++;
        s[top] = var;
    }

    int pop()
    {
        int var = s[top];
        top--;
        return var;
    }
};

int main()
{
    std::cout << "vvedite razmer massiva:";
    std::cin >> N;
    Stack a;
    a.push(5);
    a.push(7);
    a.push(7);
    a.push(9);

    std::cout << a.pop() << '\n';
    std::cout << a.pop() << '\n';
    std::cout << a.pop() << '\n';
    std::cout << a.pop() << '\n';

    return 0;
}