
#include <iostream>
#include <string>

std::string a = "default_animal_voice";

class Animal
{
private:
   
public: 
    virtual void Voice()
    {
        std::cout << a << "\n";
    }
};

class Cat : public Animal
{
private:
    std::string a = "meow-meow";
public:
    void Voice()
    {
        std::cout << a << "\n";
    }
};

class Dog : public Animal
{
private:
    std::string a = "woof-woof";
public:
    void Voice()
    {
        std::cout << a << "\n";
    }
};

class Mouse : public Animal
{
private:
    std::string a = "squeak-squeak";
public:
    void Voice() 
    {
        std::cout << a << "\n";
    }
};


int main()
{
   /*
   Animal b;
    b.Voice();
    Cat* c = new Cat;
    c->Voice();
    Dog* d = new Dog;
    d->Voice();
    Mouse* e = new Mouse;
    e->Voice();
    */

    Animal* Animals[3];
    Animals[0] = new Cat();
    Animals[1] = new Dog();
    Animals[2] = new Mouse();

    for (Animal* b : Animals)
    {
        b->Voice();
    }

}

